// homework.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void selectivePrint(bool isEven, int N) {

    if (isEven)
        std::cout << "Even";
    else
        std::cout << "Odd";

    std::cout << " values from 0 to " << N << " are : ";
   
    for (int i = 0 + !isEven; i < N; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}


int main()
{
    const int N = 5;

    selectivePrint(true, N);
    selectivePrint(false, N);
}

